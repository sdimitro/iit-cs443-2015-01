(ns greduce.core
  (:require [clojure.core.match :refer [match]])
  (:gen-class))

(defn variable [x]
  (match @x
         [:lambda z _]  z
         [:var    z]    z
         nil))

(defn body [x]
  (match @x
         [:lambda _ m]  m
         nil))

(defn instantiate [v b e2]
  (match @b
         [:var z]  (if (= z v) e2 (atom [:var z]))
         [:app m n] (atom [:app (instantiate v m e2)
                           (instantiate v n e2)])
         [:lambda z m] (if (= z v) (atom [:lambda z m]) b)))

(defn eval [code]
  (match @code
         [:app e1 e2]
         (let [_    (eval e1)
               inst (instantiate (variable e1)
                                 (body e1)
                                 e2)]
           (reset! code @inst))

         x x))

(def p1 [:app [:lambda 'x [:var 'x]] [:lambda 'y [:var 'y]]])

(defn atomize [code]
  (match code
         [:lambda z m] (atom [:lambda z (atomize m)])
         [:app m n]    (atom [:app (atomize m) (atomize n)])
         [:var x]      (atom [:var x])))

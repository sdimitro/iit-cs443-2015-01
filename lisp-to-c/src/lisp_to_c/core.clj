(ns lisp-to-c.core
  (:require [clojure.edn :as edn])
  (:gen-class))

(defn what-kind [x]
  (let [f (first x)]
    (cond (integer? f) "An integer."
          (symbol? f) (str "The symbol " f)
          (seq? f)    (str "A subexpression.")
          :elsa       (str "A something else."))))

(declare compile-exp)

(defn let-to-def [xx]
  (if (empty? xx) '()
      (let [the-var (first xx)
            the-exp (compile-exp (second xx))]
        (cons 
         (str "int " the-var " = " the-exp ";\n")
         (let-to-def (rest (rest xx)))))))

(defn compile-subexp [e]
  (let [op (first e)
        args (mapv compile-exp (rest e))]
    (cond (#{'< '> '== '<= '>= '+ '- '* '/} op)
          (str "(" (apply str (interpose (str " " op " ") args)) ")")
          (= op 'if)
          (str "( " (args 0) " ? " (args 1) " : " (args 2) ")")
          (= op 'let)
          

          )))

(defn compile-exp [e]
  (cond (integer? e) (str e)
        (symbol? e)  (str e)
        (seq? e) (compile-subexp e)
        :else  "0"))

(defn compile-def [xx]
  (assert (= (first xx) 'def) "You idiot!")
  (let [the-var (second xx)
        the-exp (compile-exp (nth xx 2))]
    (str "int " the-var " = " the-exp ";\n")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(ns grammars.core
  (:require [clojure.core.typed :as t])
  (:gen-class))

(def ex1 {'S [['x 'S 'x]
              [:eps]]})

(def ex2 {'S [['A 'x]
              ['B 'y]
              ['z]]
          'A [['1 'C 'B]
              ['2 'B]]
          'B [['3 'B]
              ['C]]
          'C [['4]
              [:eps]]})

(def fs1 {'S {:terms #{'x}
              :eps false}
          'E {:terms #{'y}
              :eps true}})

(defn join-fs [fs1 fs2]
  {:terms (into (:terms fs1) (:terms fs2))
   :eps (and (:eps fs1) (:eps fs2))}) 

(defn join-fs-eps [fs1 fs2]
  {:terms (into (:terms fs1) (:terms fs2))
   :eps (or (:eps fs1) (:eps fs2))})

(defn first-set-of [rule fs-map]
  (loop [r rule
         acc  {:terms [] :eps true}]
    (if (empty? r) acc
        (let [s (first r)
              fs (fs-map s)]
          (cond ;; No more symbolsjkk
                (nil? s)  acc
                ;; Symbol is epsilon
                (= :eps s)
                (join-fs acc {:terms #{} :eps true})
                ;; Symbol is terminal.
                (nil? fs) (join-fs acc {:terms #{s} :eps false})
                ;; Epsilon.. go down the line
                (:eps fs) (recur (rest rule)
                                 (join-fs acc fs))
                ;; No epsilon.  Join and go home.
                :elsa     (join-fs acc fs))))))

(defn gen-first-set [grm]
  (loop [fs 
         (reduce #(assoc %1 %2 {:terms #{} :eps false})
                 {}
                 (keys grm))]
    (let [nu-fs 
          (reduce (fn [fs1 sym]
                    (reduce (fn [fs2 rule]
                              (update-in fs2 [sym] #(join-fs-eps %
                                                                 (first-set-of rule fs2))))
                            fs1
                            (grm sym)))  
                  fs
                  (keys fs))]
      (if (= fs nu-fs) nu-fs
          (recur nu-fs)))))


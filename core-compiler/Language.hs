{-# LANGUAGE NoMonomorphismRestriction #-}
module Language  where

import Text.Parsec
import Text.Parsec.Expr

-- Initial Types
-- These are from chapter 2

data Expr a = ENum Int
            | EVar Name
            | EConstr Int Int
            | EAp (Expr a) (Expr a)
            | ELet IsRec
                   [(a, Expr a)]
                   (Expr a)
            | ECase (Expr a)
                    [Alter a]
            | ELam [a] (Expr a)
   deriving (Show)

type CoreExp = Expr Name
type Name = String

type IsRec = Bool
recursive = True
nonRecursive = False

bindersOf defns = [name | (name,rhs) <- defns]
rhssOf defns = [rhs | (name,rhs) <- defns]

type Alter a = (Int,[a],Expr a)
type CoreAlt = Alter Name

isAtomicExpr (EVar v) = True
isAtomicExpr (ENum n) = True
isAtomicExpr _        = False

type Program a = [ScDefn a]
type CoreProgram = Program Name

type ScDefn a = (Name,[a],Expr a)
type CoreScDefn = ScDefn Name

----------------------------------------
-- Parser: Lexical Definitions
--

reservedWords = [ "in", "let", "letrec", "of", "case" ]

var = try $
   do v <- letter
      r <- many alphaNum
      spaces
      let name = v:r
      if name `elem` reservedWords
       then fail "a keyword"
       else return name

num = do
   d <- many1 digit
   spaces
   return (read d)

symbol s =
   do r <- string s
      spaces
      return r

----------------------------------------
-- Parser: Grammar Definitions
-- Presented in the same order as figure 1.1
-- Grammar has been stratified to enable operator precedence.
-- Function application has highest precedence.

program = do
   scdefs <- sepBy1 scDef (symbol ";")
   return (scdefs :: Program String)

numExp =
   do d <- num
      return $ ENum d

varExp = 
   do v <- var
      return (EVar v)


scDef = do
   sc <- var
   args <- many var
   symbol "="
   body <- expr
   return ((sc,args,body) :: ScDefn String)

pack =
   do symbol "Pack{"
      n1 <- many1 digit
      symbol ","
      n2 <- many1 digit
      symbol "}"
      return $ EConstr (read n1) (read n2)

parens p =
   do symbol "("
      r <- p
      spaces
      symbol ")"
      return r

aExp =   try pack
     <|> varExp
     <|> numExp
     <|> parens expr

expr =
       letExp
   <|> caseExp
   <|> lambdaExp
   <|> boolExp

letExp = do
   rec <- letSym
   d <- defs
   symbol "in"
   b <- expr
   return $ ELet rec d b

caseExp = do
   symbol "case"
   e <- expr
   symbol "of"
   a <- alts
   return $ ECase e a

letSym = do { try (symbol "letrec") ; return recursive }
     <|> do { try (symbol "let") ; return nonRecursive }

alts = sepBy1 alt (symbol ";")

alt = do
   num <- num
   vars <- many var
   symbol "->"
   exp <- expr
   return ((num, vars, exp) :: Alter String)

apExp = do
   e1 <- many1 aExp
   return $ foldl1 EAp e1

defs = sepBy1 def (symbol ";")

def = do
   v <- var
   symbol "="
   e <- expr
   return (v,e)

lambdaExp = do
   symbol "\\"
   vs <- many1 var
   symbol "."
   e <- expr
   return $ ELam vs e

opify sym =
   do op <- symbol sym
      return $ (\e1 e2 -> EAp (EAp (EVar op) e1) e2)

inf sym assoc = Infix (opify sym) assoc

opTable =
  [[inf "*" AssocRight, inf "/" AssocNone],
   [inf "+" AssocRight, inf "-" AssocNone],
   [inf s AssocNone | s <- ["==", "~=", ">", ">=", "<", "<="]],
   [inf "&" AssocRight],
   [inf "|" AssocLeft]]

boolExp = buildExpressionParser opTable apExp

parse x = case Text.Parsec.parse program "stdin" x of
            Right p -> p
            Left e -> error $ show e

----------------------------------------
-- Standard Prelude

preludeDefs =
   [ ("I", ["x"], EVar "x")
   , ("K", ["x","y"], EVar "x")
   , ("K1", ["x","y"], EVar "y")
   , ("S", ["f","g","x"], EAp (EAp (EVar "f") (EVar "x"))
                              (EAp (EVar "g") (EVar "x")))
   , ("compose", ["f","g","x"], EAp (EVar "f")
                                   (EAp (EVar "g") (EVar "x")))
   , ("twice", ["f"], EAp (EAp (EVar "compose") (EVar "f")) (EVar "f"))
   ]

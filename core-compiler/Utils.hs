module Utils where

import Data.IntMap as I
import Data.HashMap.Strict as H

data Heap a = Heap Int [Int] (I.IntMap a)

instance Show x => Show (Heap x) where
  show (Heap a b c) = "Heap - Size: " ++ (show a) ++ "\n" ++
                      "   Freelist: " ++ (show (take 10 b)) ++ "\n" ++
                      "   Content: " ++ (show c) ++ "\n\n"

hInitial :: Heap a
hInitial = Heap 0 [1..] I.empty

hAlloc  (Heap size (next:free) cts) a =
     (Heap (size+1) free (I.insert next a cts), next)
hUpdate (Heap size free cts) a n =
     Heap size free (I.adjust (const n) a cts)
hFree   (Heap size free cts) a =
     Heap (size-1) (a:free) (I.delete a cts)
hLookup (Heap _    _    cts) a = findWithDefault
    (error $ "Node not found in heap: " ++ show a)
    a cts

mapAccuml f acc []     = (acc, [])
mapAccuml f acc (x:xs) = (finalacc, y : ys)
   where (acc', y) = f acc x
         (finalacc, ys) = mapAccuml f acc' xs

data Environment k v = Environment (H.HashMap k v)
  deriving Show

eInitial :: Environment k v
eInitial = Environment $ H.empty

eAdd (Environment h) k v = Environment $ H.insert k v h
eLookup (Environment h) k = H.lookup k h
eFromList xx = Environment $ H.fromList xx
eMerge (Environment e1)
       (Environment e2) = Environment $ H.union e1 e2

eSize (Environment env) = H.size env

eMap (Environment env) f = Environment $ H.map f env

eDomain (Environment env) = H.keys env

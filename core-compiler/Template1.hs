import Utils
import Language
--import Text.Parsec (parse)
import System.Console.Haskeline

data TiState = TiState { tiStack :: TiStack, 
                         tiDump :: TiDump, 
                         tiHeap :: TiHeap, 
                         tiGlobals :: TiGlobals, 
                         tiStats :: TiStats }
   deriving Show

type TiStack = [Addr]

data TiDump = DummyTiDump
   deriving Show

initialTiDump = DummyTiDump

type TiHeap = Heap Node

data Node = NAp Addr Addr
          | NSupercomb Name [Name] CoreExp
          | NNum Int
   deriving Show
type Addr = Int

type TiGlobals = Environment String Addr

type TiStats = Int

tiStatInitial :: TiStats
tiStatInitial = 0
tiStatIncSteps s = s + 1
tiStatGetSteps s = s

applyToStats :: (TiStats -> TiStats) -> TiState -> TiState
applyToStats stats_fun state = state { tiStats = stats_fun $ tiStats state }

compile program = TiState
      initial_stack 
      initialTiDump 
      initial_heap 
      globals 
      tiStatInitial
   where
      sc_defs = program ++ preludeDefs ++ extraPreludeDefs
      (initial_heap, globals) = buildInitialHeap sc_defs
      initial_stack = [address_of_main]
      address_of_main = maybe (error "main is not defined") id $ eLookup globals "main"

extraPreludeDefs = []

buildInitialHeap :: [CoreScDefn] -> (TiHeap, TiGlobals)
buildInitialHeap sc_defs = (tiheap, eFromList globlist)
    where (tiheap, globlist) = mapAccuml allocateSc hInitial sc_defs

allocateSc :: TiHeap -> CoreScDefn -> (TiHeap, (Name, Addr))
allocateSc heap (name, args, body) = (heap', (name, addr))
   where (heap', addr) = hAlloc heap (NSupercomb name args body)

---- template evaluator
--

eval state = state : rest_states
   where rest_states = if tiFinal state then [] else eval next_state
         next_state = doAdmin (step state)

doAdmin :: TiState -> TiState
doAdmin state = applyToStats tiStatIncSteps state

tiFinal :: TiState -> Bool
tiFinal state = case tiStack state of
              [x] -> isDataNode (hLookup (tiHeap state) x)
              []  -> error "Empty Stack!"
              _   -> False

isDataNode :: Node -> Bool
isDataNode (NNum n) = True
isDataNode _        = False

-- Stepping

step :: TiState -> TiState

step state =
   dispatch (hLookup (tiHeap state) (head (tiStack state)))
 where
   dispatch (NNum n) = numStep state n
   dispatch (NAp a1 a2) = apStep state a1 a2
   dispatch (NSupercomb sc args body) = scStep state sc args body

numStep :: TiState -> Int -> TiState
numStep state n = error "Number applied as a function!"

apStep :: TiState -> Addr -> Addr -> TiState
apStep state a1 a2 = state { tiStack = a1 : tiStack state }

scStep :: TiState -> Name -> [Name] -> CoreExp -> TiState
scStep state sc_name arg_names body =
   state { tiStack = new_stack, tiHeap = new_heap }
  where
   new_stack = result_addr : drop (length arg_names + 1) stack
   (new_heap, result_addr) = instantiate body heap env
   env = Prelude.foldr (\ (k,v) h -> eAdd h k v) globals arg_bindings
   arg_bindings = zip arg_names (getargs heap stack)
   stack = tiStack state
   heap = tiHeap state
   globals = tiGlobals state

getargs :: TiHeap -> TiStack -> [Addr]
getargs heap (sc:stack) = Prelude.map get_arg stack
  where get_arg addr = arg
          where NAp fun arg = hLookup heap addr

instantiate :: CoreExp -> TiHeap -> Environment Name Addr -> (TiHeap, Addr)
instantiate (ENum n) heap env = hAlloc heap (NNum n)
instantiate (EVar v) heap env =
   (heap, maybe (error $ "Undefined name " ++ show v) id (eLookup env v))
instantiate (EAp e1 e2) heap env =
   hAlloc heap2 (NAp a1 a2)
  where
   (heap1, a1) = instantiate e1 heap  env
   (heap2, a2) = instantiate e2 heap1 env
instantiate (EConstr tag arity) heap env =
   instantiateConstr tag arity heap env
instantiate (ELet isrec defs body) heap env =
   instantiateLet isrec defs body heap env
instantiate (ECase e alts) heap env = error "Can't do case."

instantiateConstr tg arity heap env = error "Can't do constructors."
instantiateLet isrec defs body heap env = error "Can't do let."

main = runInputT defaultSettings $ do
   input <- getInputLine "> "
   let p = maybe (error "No input.") parse input
   let st = compile p
   outputStrLn ("Compiled program: " ++ (show st))
   outputStrLn "Running."
   let st' = eval st
   let lst = last st'
   outputStrLn "\n\nFinal State:\n\n"
   outputStrLn (show lst)
   outputStrLn "\n\nResult:\n\n"
   -- putStrLn (show (hLookup (head (pi15 lst)) (pi35 lst)))
   let haddr = head (tiStack lst)
   outputStrLn $ show (hLookup (tiHeap lst) haddr)


test1 = "main = S K K 3"


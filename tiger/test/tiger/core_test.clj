(ns tiger.t_parse
  (:use [midje.sweet]
        [tiger.parse]))

(facts "About parsing variable declarations."
       (fact "It can parse simple variable declarations with integers."
             (parse-tiger "var x := 412")
             => [:S [:dec [:vardec "x" [:int 412]]]]
             (parse-tiger "var x : int := 4123")
             => [:S [:dec [:tvardec "x" "int" [:int 4123]]]]
             (parse-tiger "var a := 10
                           var b := 20")
             => [:S [:dec [:vardec "a" [:int 10]]]
                 [:dec [:vardec "b" [:int 20]]]]))


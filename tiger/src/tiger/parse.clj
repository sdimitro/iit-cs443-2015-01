(ns tiger.parse
  (:require [clojure.edn :as edn]
            [instaparse.core :as inst]))

(defn xformer [t]
  (inst/transform {:exp (fn [e] e)
                   :int (fn [i] [:int (edn/read-string i)])
                   :parensexp (fn [pe] pe)
                   :tyfield (fn [x y] [x y])
                   :id (fn [x] x)
                   :tyfields (fn [& rest] (vec rest))
                   :kwarrayof (fn [id] [:array id])
                   :A (fn [& ints] (apply + ints))
                   } t))

(def parser
  (inst/parser
   "S = {dec}

    dec = vardec 
        | tvardec
        | fundec
        | tfundec
        | tydec

    vardec =  <kwvar> id <assigned> exp
    tvardec = <kwvar> id <colon> id <assigned> <ws> exp
    fundec = <kwfunction> <ws> id <lp> tyfields <rp> <equal> exp
    tfundec = <kwfunction> <ws> id <lp> tyfields <rp> <colon> id <equal> exp
    tydec = <kwtype> id <equal> <kwarray> kwarrayof
          | <kwtype> id <equal> <lb> tyfields <rb>
          | <kwtype> id <equal> id

    exp = parensexp
        | seqexp
        | int
        | string
        | varexp

    seqexp = <lp> exp <scolon> exp { <scolon> exp } <rp> <ws*>
    parensexp = <lp> exp <rp> <ws*>
    int = #'[0-9]+' <ws*>
    string = <dquotes> #'[[a-z0-9]|\\s]*' <dquotes> <ws*>
    varexp = #'[a-z][a-z0-9]*' { <dot> field { <lB> arraysub <rB> } } <ws*>
           | #'[a-z][a-z0-9]*' { <lB> arraysub <rB> } <ws*>
           | id <ws*>

    field = id
    arraysub = exp

    tyfields = Epsilon
             | tyfield { <comma> tyfield }
    tyfield = id <colon> id

    kwvar = 'var' <ws+>
    kwfunction = 'function'
    kwtype = 'type' <ws+>
    kwarray = 'array' <ws+> 'of' <ws+>
    kwarrayof = id

    lb = '{' <ws>
    rb = '}' <ws>
    lB = '[' <ws>
    rB = ']' <ws>
    lp = '(' <ws>
    rp = ')' <ws>
    assigned = ':=' <ws>
    equal = '=' <ws>
    colon = ':' <ws>
    scolon = ';' <ws>
    comma = ',' <ws>
    dot = '.' <ws>
    id = #'[a-z][a-z0-9]*' <ws>
    dquotes = '\"'

    ws = #'\\s*'  ")
  )

(defn parse-tiger
  [input]
  (-> input
      parser
      xformer))


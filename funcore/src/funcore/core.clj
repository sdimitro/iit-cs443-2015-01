(ns funcore.core
  (:require [clojure.core.typed :as t])
  (:gen-class))

;;; This module defines `core`, a Haskell-like extension to lambda calculus.

(t/defalias Alter '[t/Int String Exp])

(t/defalias Exp
         (t/Rec [Exp]
                (t/U '[':var String]
                     '[':num Number]
                     '[':const t/Int t/Int]
                     '[':ap Exp Exp]
                     '[':let t/Bool (t/Vec (t/HVec [String Exp])) Exp ]
                     '[':case Exp (t/Vec Alter)]
                     '[':lam (t/Vec String) Exp])))

(t/defn ENum [i :- Number] :- Exp
  [:num i])
(t/defn EVar [x :- String] :- Exp
  [:var x])
(t/defn EConst [n :- t/Int p :- t/Int] :- Exp
  [:const n p])
(t/defn EAp [e1 :- Exp  e2 :- Exp] :- Exp
  [:ap e1 e2])
(t/defn ELet [defs :- (t/Vec (t/HVec [String Exp]))
              body :- Exp] :- Exp
              [:let false defs body])
(t/defn ELetRec [defs :- (t/Vec (t/HVec [String Exp]))
                 body :- Exp] :- Exp
                 [:let true defs body])

(t/def e :- Exp (EVar "x"))

(t/def e2 :- Exp
  [:ap [:ap [:lam ["x" "y"] [:var "x"]] [:num 10]] [:num 20]])

(ns funcore.mark1
  (:require [clojure.core.typed :as t]
            [funcore.core :as core])
  (:gen-class))

(t/defalias Node
         (t/Rec [Node]
                (t/U '[':NAp Node Node]
                     '[':NSup String]
                     '[':NNum t/Int])))

(t/defalias Address t/Int)
(t/defalias Stack (t/Vec t/Int))
(t/defalias Dump (t/Vec (t/Vec t/Int)))
(t/defalias Heap (t/Map t/Int Node))
(t/defalias Globals (t/Map String SC-Def))
(t/defalias SC-Def '[':SC String (t/Vec String) core/Exp])

(t/defalias Machine '{:stack Stack
                      :dump Dump
                      :heap Heap
                      :globals Globals})

(t/defn init-machine [] :- Machine
  {:stack []
   :dump []
   :heap {}
   :globals {}})

